package com.toopa2002.ciba;

import okhttp3.*;
import okio.Buffer;
import okio.BufferedSource;
import sun.net.www.http.HttpClient;

import java.io.IOException;

import static java.util.Objects.requireNonNull;

/*
curl -k --request POST \
--header "authorization: Basic bXlDSUJBQ2xpZW50OmZvcmdlcm9jaw==" \
--data "request=eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJsb2dpbl9oaW50IjoiNjY4Njc3Mzk5NzIiLCJzY29wZSI6Im9wZW5pZCBwcm9maWxlIiwiYWNyX3ZhbHVlcyI6ImNpYmEiLCJpc3MiOiJteUNJQkFDbGllbnQiLCJhdWQiOiJodHRwczovL3NhbmRib3gueGwudG9vcGEyMDAyLnRoZXdvcmtwYy5jb206MTA0NDUvb3BlbmFtL29hdXRoMi9yZWFsbXMvcm9vdC9yZWFsbXMveGwiLCJleHAiOjE2MDA5NDA2MTEsImJpbmRpbmdfbWVzc2FnZSI6IkFsbG93IEV4YW1wbGVCYW5rIHRvIHRyYW5zZmVyIn0.Jy0YuQCZVh5ZK6N-wPnGkzVOrJDdgB1KVxAXsWqq3o8W393ikAHmgExS3KEyPYPDDZHG1dWI0Amw2rkJDjAjhdSwSk9555VTSCtXAGF_bQqzCmYvMy80Jq6Q822_sOfNpEFRrpHaIs7cgdT7h0reX5oCMdZMvY7hdEE2xAgbQ2l8l-njVanrgLiwRshIVUq4807aPPH3RvVu8URfHdfiEAPXjArRC7WOCMoFob74fWz8YysY6CeN8kxAZUZfNYnGQujLuwPhGtlwZLWHM0UEKC9NgcWxefk0ZHoT2av-cv5wwR7TDWHiNloBxqzhU_2xKk8dEs0KA-MXbN95MKKS-Q" \
"https://sandbox.xl.toopa2002.theworkpc.com:10445/openam/oauth2/realms/root/realms/xl/bc-authorize"



curl --request POST \
--header 'authorization: Basic bXlDSUJBQ2xpZW50OmZvcmdlcm9jaw==' \
--data grant_type=urn:openid:params:grant-type:ciba \
--data auth_req_id=iu0c5UfS1Ukq1Bv1Ro5bXvnV5WU \
https://sandbox.xl.toopa2002.theworkpc.com:10445/openam/oauth2/realms/root/realms/xl/access_token

 */
public class CIBAHttpClient {
    private OkHttpClient okClient;
    private String amUrl;
    private String realm;
    private String clientId;
    private String clientPassword;
    public CIBAHttpClient(String amUrl,String realm,String clientId,String clientPassword){
        this.amUrl=amUrl;
        this.realm=realm;
        this.clientId=clientId;
        this.clientPassword=clientPassword;
        this.okClient = new OkHttpClient.Builder()
                .addInterceptor(new Interceptor() {
                    private ResponseBody copy(ResponseBody body) throws IOException {
                        BufferedSource source = body.source();

                        Buffer bufferedCopy = source.buffer().clone();
                        return ResponseBody.create(body.contentType(), body.contentLength(), bufferedCopy);
                    }
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request request= chain.request();
                        System.out.println("====== HTTP Request: ");
                        System.out.println(request);
                        System.out.println(request.headers());
                        final Buffer buffer = new Buffer();
                        request.body().writeTo(buffer);
                        System.out.println(buffer.readUtf8());


                        Response response=chain.proceed(chain.request());
                        System.out.println("====== HTTP Response: ");
                        System.out.println(response.toString()+"\n");
                        System.out.println(response.headers());
                        ResponseBody resBody=this.copy(response.body());
                        System.out.println(resBody.string());


                        return response;
                    }
                })
                .build();


    }


    public String sendBCAuthorize(String requestJwt) throws IOException {

        String url=this.amUrl+"/oauth2";
        if(this.realm!=null){
            url+="/realms/root/realms/"+this.realm;
        }
        url+="/bc-authorize";
        RequestBody body=new FormBody.Builder()
                .add("request",requestJwt)
                .build();


        Request request=new Request.Builder().url(url)
                .addHeader("authorization",Credentials.basic(clientId,clientPassword))
                .post(body)
                .build();

        Response response=okClient.newCall(request).execute();
        return response.body().string();
    }

    public String sendGetAccessToken(String authReqId) throws IOException {
        String url=this.amUrl+"/oauth2";
        if(this.realm!=null){
            url+="/realms/root/realms/"+this.realm;
        }
        url+="/access_token";
        RequestBody body=new FormBody.Builder()
                .add("grant_type","urn:openid:params:grant-type:ciba")
                .add("auth_req_id",authReqId)
                .build();



        Request request=new Request.Builder().url(url)
                .addHeader("authorization",Credentials.basic(clientId,clientPassword))
                .post(body)
                .build();

        Response response=okClient.newCall(request).execute();


        return response.body().string();
    }
}

