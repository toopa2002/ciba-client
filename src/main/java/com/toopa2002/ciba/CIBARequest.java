package com.toopa2002.ciba;

import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;

import java.security.PrivateKey;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class CIBARequest {
    /*
    {
  "login_hint": "66867739972",
  "scope": "openid profile",
  "acr_values": "ciba",
  "iss": "myCIBAClient",
  "aud": "https://sandbox.xl.toopa2002.theworkpc.com:10445/openam/oauth2/realms/root/realms/xl",
  "exp": 1600940611,
  "binding_message": "Allow ExampleBank to transfer"
}
    * */
    private String login_hint;
    private String scope;
    private String acr_values;
    private String aud;
    private String iss;
    private long exp;
    private String binding_message;

    public CIBARequest(String login_hint,String scope,String acr_values,String aud,String iss,String binding_message,int lifetime){
        this.login_hint=login_hint;
        this.scope=scope;
        this.acr_values=acr_values;
        this.aud=aud;
        this.iss=iss;
        this.binding_message=binding_message;
        Calendar calendar=Calendar.getInstance();
        calendar.add(Calendar.SECOND,lifetime);
        this.exp=calendar.getTime().getTime();
    }

    public String toJWT(PrivateKey privateKey){
        Map<String,Object> claimMap=new HashMap<>();
        claimMap.put("login_hint",login_hint);
        claimMap.put("scope",scope);
        claimMap.put("acr_values",acr_values);
        claimMap.put("binding_message",binding_message);
        JwtBuilder builder= Jwts.builder();
        return builder.setIssuer(iss)
                .setAudience(aud)
                .setExpiration(new Date(exp))
                .addClaims(claimMap)
                .signWith(privateKey)
                .compact();
    }
    public String toJSON() throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(this);
    }

    public String getLogin_hint() {
        return login_hint;
    }

    public String getScope() {
        return scope;
    }

    public String getAcr_values() {
        return acr_values;
    }

    public String getAud() {
        return aud;
    }

    public long getExp() {
        return exp;
    }

    public String getBinding_message() {
        return binding_message;
    }

    public String getIss() {
        return iss;
    }
}
