package com.toopa2002.ciba;

import org.bouncycastle.util.io.pem.PemReader;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

public class RSAKeysUtil {

    public static final String PRIVATE_PEM_FILE = "rsa256_private.pem";
    public static final String PUBLIC_PEM_FILE = "rsa256_public.pem";


    private static File loadResourceFile(String name) throws URISyntaxException {
        URL resource = RSAKeysUtil.class.getClassLoader().getResource(name);
        if (resource == null) {
            throw new IllegalArgumentException("file not found!");
        } else {

            // failed if files have whitespaces or special characters
            //return new File(resource.getFile());

            return new File(resource.toURI());
        }
    }

    public static PrivateKey loadPrivateKey() throws IOException {
        try {
            PemReader pemReader = new PemReader(new FileReader(loadResourceFile(PRIVATE_PEM_FILE)));
            PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(pemReader.readPemObject().getContent());
            return KeyFactory.getInstance("RSA").generatePrivate(spec);
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new IOException(ex);
        }

    }


    public static PublicKey loadPublicKey() throws IOException{
        try {
            PemReader pemReader = new PemReader(new FileReader(loadResourceFile(PUBLIC_PEM_FILE)));
            X509EncodedKeySpec spec = new X509EncodedKeySpec(pemReader.readPemObject().getContent());

            return KeyFactory.getInstance("RSA").generatePublic(spec);
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new IOException(ex);
        }
    }

}
