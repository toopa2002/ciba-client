package com.toopa2002.ciba;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.impl.crypto.JwtSigner;

import java.io.IOException;
import java.io.InputStream;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.HashMap;
import java.util.Properties;

public class CIBAClient {

    private static final String LOGIN_HINT="66867739972";
    private static final String SCOPES="profile openid";
    private static final String ACR_VALUES="ciba";
    private static final String AM_URL="https://sandbox.xl.toopa2002.theworkpc.com:10445/openam";
    private static final String REALM="xl";
    private static final String CLIENT_ID="myCIBAClient";
    private static final String CLIENT_SECRET="forgerock";
    private static final String BINDING_MESSAGE="binding_message";

    public static void main(String[] args) throws IOException, InterruptedException {
        String aud=AM_URL+"/oauth2";
        if(REALM!=null && !REALM.isEmpty()){
            aud+="/realms/root/realms/"+REALM;
        }
        PrivateKey privateKey= RSAKeysUtil.loadPrivateKey();
        PublicKey publicKey= RSAKeysUtil.loadPublicKey();

        CIBARequest cibaRequest=new CIBARequest(
                LOGIN_HINT,
                SCOPES,
                ACR_VALUES,
                aud,
                CLIENT_ID,
                BINDING_MESSAGE,
                600
                );

        ObjectMapper objectMapper = new ObjectMapper();

//        String request =objectMapper.writeValueAsString(cibaRequest);

        String requestJwt=cibaRequest.toJWT(privateKey);
        System.out.println("request="+cibaRequest.toJSON());

        CIBAHttpClient httpClient=new CIBAHttpClient(
                AM_URL,
                REALM,
                CLIENT_ID,
                CLIENT_SECRET);
        String bcAuthResponse=httpClient.sendBCAuthorize(requestJwt);



        HashMap<String,Object> bcAuthResponseMap=objectMapper.readValue(bcAuthResponse, HashMap.class);
        System.out.print("bcAuthResponseMap: "+bcAuthResponseMap);

        if(bcAuthResponseMap.containsKey("auth_req_id")){
            String authReqId=(String) bcAuthResponseMap.get("auth_req_id");
            HashMap<String, Object> accessTokenResponseMap;
            String pollErrorCode=null;
            do {

                System.out.println("==== Polling for auth_req_id="+authReqId);
                int interval = (int) bcAuthResponseMap.get("interval");

                System.out.println("=== waiting for " + interval + " sec");
                Thread.sleep(interval * 1000);
                String accessTokenResponse = httpClient.sendGetAccessToken(authReqId);


                accessTokenResponseMap = objectMapper.readValue(accessTokenResponse, HashMap.class);
                System.out.println("accessTokenResponseMap: "+accessTokenResponseMap);
                pollErrorCode=(String)accessTokenResponseMap.get("error");

            }while("authorization_pending".equals(pollErrorCode));

            if(accessTokenResponseMap.containsKey("access_token")){
                String access_token=(String)accessTokenResponseMap.get("access_token");
                System.out.println("\n!!!Success ");

                System.out.println("access_token="+access_token);


            }


        }
    }


}
