## DEMO Application for CIBA
### Run
-   CIBAClient is main class for running it.
-   RSA PEM files are in /resources
-   Private key: rsa256_private.pem
-   Public key: rsa256_public.pem
-   Certificate: rsa256_cert.pem